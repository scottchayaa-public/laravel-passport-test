<?php

use Illuminate\Database\Seeder;
use App\User;
use App\oauth_clients;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UserTableSeeder');
        $this->command->info('User table seeded!');

        $this->call('OauthClientsTableSeeder');
        $this->command->info('User table seeded!');
    }
}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create([
        	'name' => 'test',
        	'email' => 'test@example.com',
        	'password' => bcrypt('12345'),
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

}

class OauthClientsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('oauth_clients')->delete();

        oauth_clients::create([
        	'user_id' => '1',
        	'name' => 'test',
        	'secret' => str_random(40),
        	'redirect' => 'http://localhost',
        	'personal_access_client' => 0,
        	'password_client' => 1,
        	'revoked' => 0,
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

}